# Random avatar generator

This project will generate random avatars based on following:

- [avataaars](https://avataaars.com/)
- [ py_avataaars](https://github.com/kebu/py-avataaars)

## How to use it

This script will generate 1000 male and 1000 female avatars in avatars_output directory.

This is tested by python3. Follow following step:

```bash
python -m venv venv
source venv/bin/activate
pip install py-avataaars
mkdir -p avatars_output/male/
mkdir -p avatars_output/female/
python avatar.py

google-chrome avatar.html # open avatar.html to view the avatars in google chrome
```
