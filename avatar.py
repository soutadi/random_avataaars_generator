#!/usr/bin/env python

import py_avataaars as pa
from random import choices

skin_color = [pa.SkinColor.TANNED,
              pa.SkinColor.YELLOW,
              pa.SkinColor.PALE,
              pa.SkinColor.LIGHT,
              pa.SkinColor.BROWN,
              pa.SkinColor.DARK_BROWN,
              pa.SkinColor.BLACK]

hair_color = [pa.HairColor.BROWN,
              pa.HairColor.BLACK,
              pa.HairColor.AUBURN,
              pa.HairColor.BROWN_DARK,
              pa.HairColor.BLONDE_GOLDEN,
              pa.HairColor.PLATINUM,
              pa.HairColor.SILVER_GRAY]


facial_hair_type = [pa.FacialHairType.DEFAULT,
                    pa.FacialHairType.BEARD_MEDIUM,
                    pa.FacialHairType.BEARD_LIGHT,
                    pa.FacialHairType.BEARD_MAJESTIC,
                    pa.FacialHairType.MOUSTACHE_FANCY,
                    pa.FacialHairType.MOUSTACHE_MAGNUM]

top_type_female = [
    pa.TopType.HIJAB,
    pa.TopType.LONG_HAIR_BIG_HAIR,
    pa.TopType.LONG_HAIR_BOB,
    pa.TopType.LONG_HAIR_BUN,
    pa.TopType.LONG_HAIR_CURLY,
    pa.TopType.LONG_HAIR_CURVY,
    pa.TopType.LONG_HAIR_FRIDA,
    pa.TopType.LONG_HAIR_FRO_BAND,
    pa.TopType.LONG_HAIR_NOT_TOO_LONG,
    pa.TopType.LONG_HAIR_SHAVED_SIDES,
    pa.TopType.LONG_HAIR_MIA_WALLACE,
    pa.TopType.LONG_HAIR_STRAIGHT,
    pa.TopType.LONG_HAIR_STRAIGHT2,
    pa.TopType.LONG_HAIR_STRAIGHT_STRAND,
    pa.TopType.SHORT_HAIR_SHAGGY_MULLET
]

top_type_male = [
    pa.TopType.NO_HAIR,
    pa.TopType.EYE_PATCH,
    pa.TopType.HAT,
    pa.TopType.TURBAN,
    pa.TopType.WINTER_HAT1,
    pa.TopType.WINTER_HAT2,
    pa.TopType.WINTER_HAT3,
    pa.TopType.WINTER_HAT4,
    pa.TopType.LONG_HAIR_DREADS,
    pa.TopType.LONG_HAIR_FRO,
    pa.TopType.SHORT_HAIR_DREADS_01,
    pa.TopType.SHORT_HAIR_DREADS_02,
    pa.TopType.SHORT_HAIR_FRIZZLE,
    pa.TopType.SHORT_HAIR_SHORT_CURLY,
    pa.TopType.SHORT_HAIR_SHORT_FLAT,
    pa.TopType.SHORT_HAIR_SHORT_ROUND,
    pa.TopType.SHORT_HAIR_SHORT_WAVED,
    pa.TopType.SHORT_HAIR_SIDES,
    pa.TopType.SHORT_HAIR_THE_CAESAR,
    pa.TopType.SHORT_HAIR_THE_CAESAR_SIDE_PART
]

hair_color = choices(hair_color)[0]

clothe_type = [
    pa.ClotheType.BLAZER_SHIRT,
    pa.ClotheType.BLAZER_SWEATER,
    pa.ClotheType.COLLAR_SWEATER,
    pa.ClotheType.GRAPHIC_SHIRT,
    pa.ClotheType.HOODIE,
    pa.ClotheType.OVERALL,
    pa.ClotheType.SHIRT_CREW_NECK,
    pa.ClotheType.SHIRT_SCOOP_NECK,
    pa.ClotheType.SHIRT_V_NECK
]

clothe_graphic_type = [
    pa.ClotheGraphicType.BAT,
    pa.ClotheGraphicType.CUMBIA,
    pa.ClotheGraphicType.DEER,
    pa.ClotheGraphicType.DIAMOND,
    pa.ClotheGraphicType.HOLA,
    pa.ClotheGraphicType.PIZZA,
    pa.ClotheGraphicType.RESIST,
    pa.ClotheGraphicType.SELENA,
    pa.ClotheGraphicType.BEAR,
    pa.ClotheGraphicType.SKULL_OUTLINE,
    pa.ClotheGraphicType.SKULL
]

clothe_color = [
    pa.ClotheColor.BLACK,
    pa.ClotheColor.BLUE_01,
    pa.ClotheColor.BLUE_02,
    pa.ClotheColor.BLUE_03,
    pa.ClotheColor.GRAY_01,
    pa.ClotheColor.GRAY_02,
    pa.ClotheColor.HEATHER,
    pa.ClotheColor.PASTEL_BLUE,
    pa.ClotheColor.PASTEL_GREEN,
    pa.ClotheColor.PASTEL_ORANGE,
    pa.ClotheColor.PASTEL_RED,
    pa.ClotheColor.PASTEL_YELLOW,
    pa.ClotheColor.PINK,
    pa.ClotheColor.RED,
    pa.ClotheColor.WHITE
]

mouth_type = [
    pa.MouthType.DEFAULT,
    pa.MouthType.CONCERNED,
    pa.MouthType.DISBELIEF,
    pa.MouthType.EATING,
    pa.MouthType.GRIMACE,
    pa.MouthType.SAD,
    pa.MouthType.SCREAM_OPEN,
    pa.MouthType.SERIOUS,
    pa.MouthType.SMILE,
    pa.MouthType.TONGUE,
    pa.MouthType.TWINKLE
]

eye_type = [
    pa.EyesType.DEFAULT,
    pa.EyesType.CLOSE,
    pa.EyesType.CRY,
    pa.EyesType.DIZZY,
    pa.EyesType.EYE_ROLL,
    pa.EyesType.HAPPY,
    pa.EyesType.HEARTS,
    pa.EyesType.SIDE,
    pa.EyesType.SQUINT,
    pa.EyesType.SURPRISED,
    pa.EyesType.WINK,
    pa.EyesType.WINK_WACKY
]

eyebrow_type = [
    pa.EyebrowType.DEFAULT,
    pa.EyebrowType.DEFAULT_NATURAL,
    pa.EyebrowType.ANGRY,
    pa.EyebrowType.ANGRY_NATURAL,
    pa.EyebrowType.FLAT_NATURAL,
    pa.EyebrowType.RAISED_EXCITED,
    pa.EyebrowType.RAISED_EXCITED_NATURAL,
    pa.EyebrowType.SAD_CONCERNED,
    pa.EyebrowType.SAD_CONCERNED_NATURAL,
    pa.EyebrowType.UNI_BROW_NATURAL,
    pa.EyebrowType.UP_DOWN,
    pa.EyebrowType.UP_DOWN_NATURAL,
    pa.EyebrowType.FROWN_NATURAL
]

accessories_type = [
    pa.AccessoriesType.DEFAULT,
    pa.AccessoriesType.KURT,
    pa.AccessoriesType.PRESCRIPTION_01,
    pa.AccessoriesType.PRESCRIPTION_02,
    pa.AccessoriesType.ROUND,
    pa.AccessoriesType.SUNGLASSES,
    pa.AccessoriesType.WAYFARERS
]

for i in range(1000):
    male_avatar = pa.PyAvataaar(
        style=pa.AvatarStyle.TRANSPARENT,
        skin_color=choices(skin_color)[0],
        hair_color=hair_color,
        facial_hair_type=choices(facial_hair_type)[0],
        facial_hair_color=hair_color,
        top_type=choices(top_type_male)[0],
        hat_color=choices(clothe_color)[0],
        mouth_type=choices(mouth_type)[0],
        eye_type=choices(eye_type)[0],
        eyebrow_type=choices(eyebrow_type)[0],
        accessories_type=choices(accessories_type)[0],
        clothe_type=choices(clothe_type)[0],
        clothe_color=choices(clothe_color)[0],
        clothe_graphic_type=choices(clothe_graphic_type)[0],
    )
    male_avatar.render_svg_file('avatars_output/male/%s.svg' % i)


for i in range(1000):
    female_avatar = pa.PyAvataaar(
        style=pa.AvatarStyle.TRANSPARENT,
        skin_color=choices(skin_color)[0],
        hair_color=hair_color,
        top_type=choices(top_type_female)[0],
        hat_color=choices(clothe_color)[0],
        mouth_type=choices(mouth_type)[0],
        eye_type=choices(eye_type)[0],
        eyebrow_type=choices(eyebrow_type)[0],
        accessories_type=choices(accessories_type)[0],
        clothe_type=choices(clothe_type)[0],
        clothe_color=choices(clothe_color)[0],
        clothe_graphic_type=choices(clothe_graphic_type)[0],
    )
    female_avatar.render_svg_file('avatars_output/female/%s.svg' % i)
